# Import Packages
## Standard Packages
import pickle
import re

# ## Installed Packages
import pandas as pd
import numpy as np
import xgboost as xgb
from newspaper import Article, ArticleException
import shap
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output, State
from envparse import env
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Float
from sqlalchemy.orm import sessionmaker
from flask import Flask
from flask_restful import reqparse, Resource, Api
import requests
import tldextract


# Custom Functions
def extract_article(url):
    """
    Extracts article data from URL
    Parameters:
        url (str): Article URL
    Returns:
        (pandas.DataFrame): Data frame with article URL, title, body and publication date
    """
    try:
        article = Article(url)
        article.download()
        article.parse()
    except ArticleException:
        print("Error: Article URL cannot be reached. Please try again with another URL.")
        return None

    if article.text.strip() in ['', None]:
        print(
            "Error: This article has no parsable content, and thus a rating cannot be provided. Please try again with "
            "another URL.")
        return None
    else:
        df = pd.DataFrame(data={"url": url,
                                "text": article.text,
                                "title": article.title,
                                "publish_date": article.publish_date},
                          index=[0])
        return df


model_id = "VeriFYI__FakeNewsDetector__text_lexicon__2022-08-12"


def get_model_artifacts():
    """
    Loads model objects to memory
    Returns:
        Model object, pipeline for lexicon feature extraction
    """
    model_path = 'src/' + model_id + ".json"
    lexicon_pipeline_path = 'src/' + model_id + "__lexicon_pipeline.pickle"

    xgb_model = xgb.XGBClassifier(n_jobs=2, use_label_encoder=False, eval_metric='logloss')
    xgb_model.load_model(model_path)

    with open(lexicon_pipeline_path, 'rb') as f:
        pipe_lex = pickle.load(f)

    return xgb_model, pipe_lex


def get_model_input(df, pipe_lex):
    """
    Converts raw data into model features
    Parameters:
        df (pandas.DataFrame): Data frame with article URL, title, body and publication date
        pipe_lex: Pipeline for lexicon feature extraction
    Returns:
        (np.array): Array of model features
    """
    lex_vec = pipe_lex.transform(df["text"])
    return lex_vec


def get_model_output(model_obj, model_input):
    """
    Computes model output (probability) for given input
    Parameters:
        model_obj (xgb.XGBClassifier): Model object
        model_input (np.array): Array of model features
    Returns:
        (float): Probability of given input being classified as fake news content
    """
    pred_prob = model_obj.predict_proba(model_input)[:, 1][0]
    return float(pred_prob)


def get_model_output_class(pred_prob, optimal_threshold=0.30082986):
    """
    Converts probability of fake news content into a classification
    Parameters:
        pred_prob (float): Probability of given input being classified as fake news content
        optimal_threshold (float): Value above which a classification of fake news content is given
    Returns:

    """
    if pred_prob > optimal_threshold:
        return "Fake News Content"
    else:
        return "Not Fake News Content"


def get_important_model_features(model_obj, pipe_lex, model_input, n_top=10):
    """
    Produce table of top model features
    Parameters:
        model_obj (xgb.XGBClassifier): Model object
        pipe_lex: Pipeline for lexicon feature extraction
        model_input (np.array): Array of model features
        n_top (int): Number of top features to extract
    Returns:
        Table of top model features, sorted by SHAP values
    """
    feature_names = list(pipe_lex["count"].get_feature_names_out())
    explainer = shap.Explainer(model_obj, feature_names=feature_names)
    shap_values = explainer(model_input)
    shap_values_abs = np.abs(shap_values[0].values)
    feature_importance = pd.DataFrame(list(zip(feature_names, model_input.A.tolist()[0], shap_values_abs)),
                                      columns=['Feature', 'Feature Value', 'SHAP Value'])
    feature_importance = feature_importance.sort_values(by=['SHAP Value'], ascending=False)
    feature_importance_top = feature_importance.head(n_top)
    tbl = dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i}
                 for i in feature_importance_top.columns],
        data=feature_importance_top.to_dict('records'),
        style_cell=dict(textAlign='left'),
        style_header=dict(backgroundColor='paleturquoise'),
        style_data=dict(backgroundColor='lavender')
    )
    table_layout = html.Div([
        dcc.Markdown("According to the model SHAP values, the top {n_top} most significant words that contribute to "
                     "the model output and prediction are, in descending order of significance:".format(n_top=str(n_top)
                                                                                                        )),
        tbl
    ])
    return table_layout


def save_article_metadata(df, pred_prob):
    """
    Saves article metadata (url, title, publication date, model score) to database
    Parameters:
        df (pandas.DataFrame): Data frame with article URL, title, body and publication date
        pred_prob (float): Probability of given input being classified as fake news content
    Returns:
        None
    """
    engine = create_engine(uri, echo=True)
    Base = declarative_base()

    class ArticleData(Base):
        __tablename__ = 'article_data'
        id = Column(Integer, primary_key=True)
        url = Column(String)
        title = Column(String)
        publish_date = Column(DateTime)
        model_score = Column(Float)
        model_name = Column(String)

    Session = sessionmaker(bind=engine)
    session = Session()

    try:
        publication_date = df["publish_date"].dt.to_pydatetime()[0]
    except AttributeError:
        publication_date = None

    sample_article = ArticleData(
        url=df["url"].values[0],
        title=df["title"].values[0],
        publish_date=publication_date,
        model_score=pred_prob,
        model_name=model_id
    )

    session.add(sample_article)

    session.commit()
    session.close()
    engine.dispose()


def extract_rdap_metadata(url):
    """
    Extract domain registration dates from RDAP API (Registration Data Access Protocol)
    Parameters:
        url (str): Article URL
    Returns:
        Table of key dates for domain registration information from RDAP
    """
    # TODO: Ensure that number of requests to RDAP.org is less than 600 per rolling 300 second window

    object_type = 'domain'
    table_layout = html.Div([])

    # For URL shorteners
    try:
        url = requests.head(url).headers['location']
    except requests.exceptions.ConnectionError:
        pass
    except KeyError:
        pass

    # Remove Wayback Machine domain
    ia_domain = 'web.archive.org/'
    if ia_domain in url:
        url = re.sub('https\:\/\/web\.archive\.org\/web\/[0-9]{1,}/', '', url)

    try:
        ext = tldextract.extract(url)
        object_id = ext.registered_domain
    except:
        return table_layout

    try:
        r = requests.get('https://rdap.org/{type}/{object}'.format(type=object_type, object=object_id))
        if r.status_code == 200:
            if 'events' in r.json().keys():
                df_rdap = pd.DataFrame(r.json()['events'])
                df_rdap['eventDate'] = pd.to_datetime(df_rdap['eventDate'])
                df_rdap = df_rdap.sort_values('eventDate')
                tbl = dash_table.DataTable(
                    id='table',
                    columns=[{"name": i, "id": i}
                             for i in df_rdap.columns],
                    data=df_rdap.to_dict('records'),
                    style_cell=dict(textAlign='left'),
                    style_header=dict(backgroundColor='paleturquoise'),
                    style_data=dict(backgroundColor='lavender')
                )
                # Print table of key dates for domain registration information from RDAP
                table_layout = html.Div([
                    dcc.Markdown("* **Website Domain:** {domain}".format(domain=object_id)),
                    dcc.Markdown(
                        "**Domain registration dates from RDAP (Registration Data Access Protocol)**:"),
                    tbl
                ])
                return table_layout
            else:
                # No domain registration info dates available
                return table_layout
    except:
        return table_layout


# Dash
server = Flask('app')
app = dash.Dash(__name__, server=server, title="Veri.FYI - PressDB Article Fake News Detection")

# API
api = Api(server)
parser = reqparse.RequestParser()
parser.add_argument('url')


class VeriFYI(Resource):
    def post(self):
        args = parser.parse_args()
        df = extract_article(url=args['url'])
        if df is None:
            return {'Error': 'Article URL cannot be reached or has no parsable content. Please try again with another '
                             'URL'}, 400
        else:
            xgb_model, pipe_lex = get_model_artifacts()

            lex_vec = get_model_input(df=df, pipe_lex=pipe_lex)

            pred_prob = get_model_output(model_obj=xgb_model, model_input=lex_vec)

            pred_prob_rounded = str(round(pred_prob, 2))

            pred_class = get_model_output_class(pred_prob)

            save_article_metadata(df=df, pred_prob=pred_prob)

            return {'score': pred_prob_rounded, 'predicted_class': pred_class}, 201


api.add_resource(VeriFYI, '/predict')


# Connect to database for storing article metadata
uri = env.str('DATABASE_URL')
if uri.startswith("postgres://"):
    uri = uri.replace("postgres://", "postgresql://", 1)

# HTML/CSS
## Note that much of this CSS comes from https://codepen.io/chriddyp/pen/bWLwgP.css
logo_image_style = {'textAlign': 'left', "display": "block",
                    "margin-left": "auto", "margin-right": "auto", 'padding-top': 20}
input_style = {'height': '38px', 'padding': '6px 10px',
               'background-color': '#fff', 'border': '1px solid #D1D1D1',
               'border-radius': '4px', 'box-shadow': 'none',
               'box-sizing': 'border-box', 'width': '900px'}
button_style = {'display': 'inline-block',
                'height': '38px',
                'padding': '0 30px',
                'color': '#555',
                'text-align': 'center',
                'font-weight': '600',
                'line-height': '38px',
                'letter-spacing': '.1rem',
                'text-decoration': 'none',
                'white-space': 'nowrap',
                'background-color': 'transparent',
                'border-radius': '4px',
                'border': '1px solid #bbb',
                'cursor': 'pointer',
                'box-sizing': 'border-box'}
div_style = {
    'font-size': '1em',
    'line-height': '0px',
    'margin': '3',
    'margin-bottom': '3rem',
    'position': 'relative',
    'top': '3rem',
    'left': '0',
    'textAlign': 'left',
    'padding-top': '1rem',
    'padding-left': '5rem'}

app.layout = html.Div([
    html.H1([html.Img(src=app.get_asset_url('PressDB_Logo.png'), style={'height': '10%', 'width': '10%'})],
            style=logo_image_style),
    html.H1("Veri.FYI: Article Fake News Detection"),
    html.Div([
        dcc.Markdown("Detect fake news websites at scale."),
        dcc.Markdown("Are you a journalist, fact-checker or researcher? We'd love to get your feedback on how we can "
                     "better serve you."),
        dcc.Markdown("Try our Veri.FYI prototype and send any questions and comments to partners \[at\] pressdb \["
                     "dot\] info.")
    ]),
    dcc.Input(id="article_url", type="url", placeholder="Enter article URL to fact-check here.", style=input_style),
    html.Br(),
    html.Button(id='submit-button', n_clicks=0, children='Submit', style=button_style),
    html.Button(id='reset-button', n_clicks=0, children='Reset', style=button_style),
    html.Br(),
    dcc.Loading(
        id='loading',
        type='circle',
        children=[html.Div(id="factcheck-output")]
    ),
    html.Br(),
    html.Div([
        dcc.Markdown("**Warning to users**: Veri.FYI is an experimental beta for detecting online fake news websites. "
                     "Here, fake news is defined as a news article that is false and intentionally misleading.⁽¹⁾ It "
                     "uses [an open source machine learning model](https://bitbucket.org/pressdb/fake_news_detector)⁽³⁾"
                     " based solely on the relative frequencies of words in an article's text, and was trained using a "
                     "limited dataset from [FakeNewsNet](https://github.com/KaiDMML/FakeNewsNet/tree/old-version).⁽²⁾ "
                     "This dataset mostly includes English language articles based on general interest news in the "
                     "United States from the mid-2010s. As such, the accuracy of this model on articles outside of "
                     "this scope is unclear. Even so, sharing feedback on which articles the model does and does not "
                     "perform well on, as well as sharing examples of fake news articles in general, are both highly "
                     "encouraged."
                     ),
        dcc.Markdown(
            "1. Kai Shu, Amy Sliva, Suhang Wang, Jiliang Tang, and Huan Liu. 2017. Fake news "
            "detection on social media: A data mining perspective. ACM SIGKDD Explorations Newsletter 19, "
            "1 (2017), 22–36."
        ),
        dcc.Markdown(
            "2. Kai Shu, Deepak Mahudeswaran, Suhang Wang, Dongwon Lee, and Huan Liu. "
            "2018. FakeNewsNet: A Data Repository with News Content, Social Context and Dynamic Information "
            "for Studying Fake News on Social Media. arXiv preprint arXiv:1809.01286 (2018)."),
        dcc.Markdown(
            "3. Xinyi Zhou, Atishay Jain, Vir V. Phoha, and Reza Zafarani. "
            "2020. Fake News Early Detection: An Interdisciplinary Study. "
            "1, 1 (September 2020), 25 pages"),
        dcc.Markdown("This work is licensed under a [GNU General Public License, version 3.0]("
                     "https://www.gnu.org/licenses/gpl-3.0.en.html).")
    ])
]
)


@app.callback(
    Output("factcheck-output", "children"),
    [Input('submit-button', 'n_clicks')],
    [State('article_url', 'value')]
)
def update_value(n_clicks, value):
    if n_clicks >= 1 and value.strip() not in ['', None]:
        df_article = extract_article(url=value)

        if df_article is None:
            submit_layout = html.Div([
                dcc.Markdown(
                    "Error: Article URL cannot be reached or has no parsable content. Please try again with another "
                    "URL.")
            ])
        else:
            rdap_layout = extract_rdap_metadata(url=value)

            xgb_model, pipe_lex = get_model_artifacts()

            lex_vec = get_model_input(df=df_article, pipe_lex=pipe_lex)

            pred_prob = get_model_output(model_obj=xgb_model, model_input=lex_vec)

            top_features_layout = get_important_model_features(model_obj=xgb_model,
                                                               pipe_lex=pipe_lex,
                                                               model_input=lex_vec, n_top=10)

            pred_prob_rounded = str(round(pred_prob, 2))

            pred_class = get_model_output_class(pred_prob)

            submit_layout = html.Div([
                dcc.Markdown("# Source"),
                dcc.Markdown("* **URL:** {url}".format(url=df_article["url"].values[0])),
                dcc.Markdown("* **Title:** {title}".format(title=df_article["title"].values[0])),
                dcc.Markdown("* **Publication Date:** {date}".format(date=df_article["publish_date"].values[0])),
                rdap_layout,
                dcc.Markdown("# Model Output"),
                dcc.Markdown(
                    "The probability of this article being fake news content is **{prob}**.".format(prob=
                                                                                                    pred_prob_rounded)),
                dcc.Markdown("This article is predicted to be **{pred_class}**.".format(pred_class=pred_class)),
                top_features_layout
            ])

            save_article_metadata(df=df_article, pred_prob=pred_prob)
        return submit_layout


# Reset user input
@app.callback(Output('article_url', 'value'),
              [Input('reset-button', 'n_clicks')])
def update(reset):
    return ''


if __name__ == '__main__':
    app.run_server(debug=False, host='0.0.0.0')
