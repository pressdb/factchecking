Fact Checking
=============

GUI Demo for automated fact-checking web platform.

Takes an article URL as input and will output a measure of reliability for each article claim along with a summarized contextual explanation of each measure value, based on the veracity of the claims made and the claim sources.
